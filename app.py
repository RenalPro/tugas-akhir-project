from save.mahasiswa import basecase
from flask import Flask, jsonify, request
import json

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False

@app.route('/')
def main():
    return "connect"

@app.route('/show')
def show():
    hasil = mysqldb.showcase()
    result = []
    for i in hasil:
        data = {
            'id' : i[0],
            'nama' : i[1],
            'namalengkap' : i[2],
            'nim' : i[3],
            'email' : i[4]
        }
        result.append(data)
    return jsonify(result)

@app.route('/insert')
def insertdata():
    hasil = mysqldb.insertcase()
    cek = mysqldb.showcase()
    result = []
    for i in cek:
        data = {
            'id' : i[0],
            'nama' : i[1],
            'namalengkap' : i[2],
            'nim' : i[3],
            'email' : i[4]
        }
        result.append(data)
    print(hasil)
    return jsonify(result) 

@app.route('/update', methods=["POST"])
def update():
    params = request.json
    hasil = mysqldb.updatecaseId(**params)
    hasil = "Berhasil mengupdate data"
    return jsonify(hasil)

@app.route('/delete', methods=["POST"])
def delete():
    params = request.json
    hasil = mysqldb.deletecaseId(**params)
    hasil = 'Data berhasil di hapus'
    return jsonify(hasil)


    

if __name__ == "__main__":
    mysqldb = basecase()
    app.run(debug=True)