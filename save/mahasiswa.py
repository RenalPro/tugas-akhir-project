from mysql.connector import connect

class basecase:

    def __init__(self):
        self.db = connect(host='localhost',
                        database='universitas',
                        user='root',
                        password='jarikuada5biji!')

    def showcase(self):
        cursor = self.db.cursor()
        query = '''
        select * from mahasiswa;
        '''
        cursor.execute(query)
        hasil = cursor.fetchall()
        return hasil
    
    def insertcase(self):
        cursor = self.db.cursor()
        sql = "INSERT INTO mahasiswa (nama, namalengkap, nim, email) VALUES (%s, %s, %s, %s)"
        values = [
        ("putra", "Jambrut putra", "998", "putra8@gmail.com" ),
        ("miku", "Hatsune miku", "091", "miku93@gmail.com")
        ]
        for val in values:
            cursor.execute(sql, val)
            self.db.commit()


    def updatecaseId(self, **params):
        userid = params['id']
        query = '''update mahasiswa
        set 
        nama = "Suneo",
        namalengkap = "Suneo King",
        email = "Suneo@gmail.com" 
        where id = {0};'''.format(userid)

        cursor = self.db.cursor()
        cursor.execute(query)
        self.db.commit()

    def deletecaseId(self, **params):
        user = params['id']
        crud_query = '''delete from mahasiswa where id = {0};'''.format(user)
        cursor = self.db.cursor()
        cursor.execute(crud_query)
        self.db.commit()
    

        
